package fr.cpe.dto.impl;

import fr.cpe.models.Role;
import fr.cpe.models.impl.User;

public class UserDTO {
	
	private String login;
	private boolean validAuth; 
	private Role role;
	
	public UserDTO(User user, boolean auth) {
		this.login = user.getLogin();
		this.validAuth = auth;
		this.role = user.getRole();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean isValidAuth() {
		return validAuth;
	}

	public void setValidAuth(boolean validAuth) {
		this.validAuth = validAuth;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
