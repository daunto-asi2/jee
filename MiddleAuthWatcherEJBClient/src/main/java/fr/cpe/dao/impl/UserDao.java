package fr.cpe.dao.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import fr.cpe.models.impl.User;

@Stateless
public class UserDao {

	@PersistenceContext
	EntityManager em;
	
	public User getUserByLogin(String login, String pwd) {
		try {
			User user = (User) em.createQuery("from User u where u.login = :login and u.pwd = :pwd")
					.setParameter("login", login)
					.setParameter("pwd", pwd)
					.getSingleResult();
			return user;
		} catch (NoResultException | NonUniqueResultException e) {
			return null;
		}
	}
	
	public User getUserByLoginMocked(String login, String pwd) {
		User user = new User();
		user.setLogin(login);
		return user;
	}
}
