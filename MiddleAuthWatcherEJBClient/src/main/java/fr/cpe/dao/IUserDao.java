package fr.cpe.dao;

import javax.ejb.Local;

import fr.cpe.models.IUser;

@Local
public interface IUserDao {

	public IUser getUserByLogin(String login, String pwd);
	
	public IUser getUserByLoginMocked(String login, String pwd);
}
