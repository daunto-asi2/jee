package fr.cpe.ejb;

import javax.ejb.Local;

import fr.cpe.models.impl.User;

@Local
public interface MessageReceiverSyncLocal {

	public User receiveMessage();
}
