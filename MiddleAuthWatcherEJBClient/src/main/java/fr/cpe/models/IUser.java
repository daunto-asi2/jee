package fr.cpe.models;

public interface IUser {
	
	public String getLogin();
	public void setLogin(String login);
	public String getPwd();
	public void setPwd(String pwd);
	public Role getRole();
	public void setRole(Role role);
}
