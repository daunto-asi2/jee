package fr.cpe.ejb;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.Topic;

import fr.cpe.models.impl.User;

@Stateless
@LocalBean
public class MessageReceiverSync implements MessageReceiverSyncLocal {

	@Inject
	JMSContext context;
	
	@Resource(mappedName = "java:/jms/queue/watcherqueue")
	Queue queue;
	
	@Override
	public User receiveMessage() {
		Message message = context.createConsumer(queue).receive(3000);
		try {
			return message.getBody(User.class);
		} catch (JMSException e) {
			return null;
		}
	}

}
