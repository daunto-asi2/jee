package fr.cpe.rest.impl;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.cpe.dto.impl.LoginDTO;
import fr.cpe.dto.impl.UserDTO;
import fr.cpe.ejb.MessageReceiverSyncLocal;
import fr.cpe.ejb.MessageSenderLocal;
import fr.cpe.models.Role;
import fr.cpe.models.impl.User;
import fr.cpe.rest.IWatcherAuthRestService;

public class WatcherAuthRestService implements IWatcherAuthRestService{
	
	@Inject
	MessageSenderLocal sender;
	
	@Inject
	MessageReceiverSyncLocal receiver;
	
	@Override
	public Response authUser(LoginDTO login) {
		if (login != null) {
			User user = new User();
			user.setLogin(login.getLogin());
			user.setPwd(login.getPwd());
			sender.sendMessage(user);
			User dbUser = receiver.receiveMessage();
			if (dbUser == null) {
//				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
				
			}
			UserDTO userDTO = new UserDTO(dbUser, dbUser.getRole() != Role.NONE);
			return Response.ok(userDTO).build();
		} else {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
	
}
